'use strict';

var _ = require('lodash');

exports = module.exports = function (val, min) {
  val = _.isObject(val) ? NaN : _.parseInt(val.toString());

  return !isNaN(val) && val >= min;
};

exports.message = 'is too small';
