'use strict';

var _ = require('lodash');
var defaults = require('./lib/rules');
var format = require('string-format');

var Atropos = function (rules) {
  rules = rules || {};
  this.rules = _.assign({}, defaults, rules);

  return this;
};

function shouldRun(rule, val, args) {
  return (rule.evaluateNull || val !== null) &&
    (rule.evaluateUndefined || val !== undefined) &&
    (rule.alwaysRun || args !== false);
}

Atropos.prototype.runRule = function (ctx, rule, val, args) {
  // if we've been given a name instead of a rule block (from eg conform), look it up
  if (!_.isPlainObject(rule)) {
    rule = this.rules[rule];

    if (!rule) { return ['invalid rule specified']; }
  }

  if (shouldRun(rule, val, args)) {
    if (args === undefined) { args = []; }
    if (rule.argArray) { args = [args]; }

    var func = rule.func || rule;   // for custom rule blocks

    var result = func.apply({
      runRule: this.runRule,
      context: ctx
    }, [val].concat(args));

    if (_.isBoolean(result) && !result) {
      return [format.apply(this, [rule.message].concat(args))];
    } else if (_.isArray(result)) { // message array from conform
      return result;
    }
  }

  return [];
};

Atropos.prototype.evaluateObject = function (ctx, schema, val, name, formMode=false) {
  var self = this;

  return _.transform(schema, function (result, args, key) {
    if (!_.isPlainObject(args)) {  // rule
      if (formMode) {
        val = val || null;  // blank form values are empty strings, convert to nulls for rule purposes
      }

      var res = {
        rule: key,
        result: self.runRule(ctx, key, val, args)
      };

      var len = res.result.length;

      if (len > 0) {
        if (self.rules[key].includeArgs !== false) { res.args = args; }

        var formatStr = len > 1 ? '{}-{}' : '{}';

        const localName = name || '*';   // nicer than "undefined" as a key for results on the root context

        result[localName] = (result[localName] || []).concat(res.result.map(function (r, idx) {
          res.rule = format(formatStr, key, idx);
          res.result = r;

          return _.clone(res);
        }));
      }
    } else if (!!val) {  // child object
      if (_.isArray(val)) {
        return val.map(function (element, idx) {
          _.mergeWith(
            result,
            self.evaluateObject(val, args, element, name + '.' + idx, formMode),
            function (a, b) {
              if (_.isArray(a)) { return a.concat(b); } // merge all results if multiple subschemata provided
              return b;
            });
        });
      } else {
        var child = name ? name + '.' + key : key;

        _.merge(result, self.evaluateObject(val, args, val[key], child, formMode));
      }
    }
  });
};

Atropos.prototype.validate = function (schema, obj, formMode) {
  var results = this.evaluateObject(obj, schema, obj, undefined, formMode);

  return {
    valid: Object.keys(results).length === 0,
    errors: results
  };
};

exports = module.exports = Atropos;
